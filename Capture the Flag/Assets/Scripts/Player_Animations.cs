﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// @Author: Bradley Cesario, 11/2/2017
/// The purpose of class Player_Animations is to allow animations from each
/// player to be synced over a network.
/// </summary>
public class Player_Animations : NetworkBehaviour
{
    private Animator anim;
    //Animation Triggers
    const string LEFT_TRIGGER = "Left";
    const string RIGHT_TRIGGER = "Right";
    const string FORWARD_TRIGGER = "Forward";
    const string BACK_TRIGGER = "Back";

    void Awake()
    {
        //Set animator on Awake
        anim = GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        //----------------NOTICE!!!!!!!!!---------------------------------------
        //Movement controls that can be removed and replaced with other controls.
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
        //----------------------------------------------------------------------

        //Inputs for changing color based on movement.
        if (Input.GetKeyDown(KeyCode.A))
        {
            CmdAnimateLeft();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            CmdAnimateRight();
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            CmdAnimateForward();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            CmdAnimateBack();
        }
    }

    //Server replicating changes to all clients.
    [ClientRpc]
    void RpcAnimatedLeft()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Left") == false)
        {
            anim.SetTrigger(LEFT_TRIGGER);
        }
    }

    [ClientRpc]
    void RpcAnimatedRight()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Right") == false)
        {
            anim.SetTrigger(RIGHT_TRIGGER);
        }
    }
        
    [ClientRpc]
    void RpcAnimatedForward()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Forward") == false)
        {
            anim.SetTrigger(FORWARD_TRIGGER);
        }
    }

    [ClientRpc]
    void RpcAnimatedBack()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Back") == false)
        {
            anim.SetTrigger(BACK_TRIGGER);
        }
    }
    //Commands sent to server requesting for the animation change.
    [Command]
    void CmdAnimateLeft()
    {
        RpcAnimatedLeft();
    }

    [Command]
    void CmdAnimateRight()
    {
        RpcAnimatedRight();
    }

    [Command]
    void CmdAnimateForward()
    {
        RpcAnimatedForward();
    }

    [Command]
    void CmdAnimateBack()
    {
        RpcAnimatedBack();
    }
}
