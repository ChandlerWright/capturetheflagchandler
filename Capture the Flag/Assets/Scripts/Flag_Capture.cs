﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Flag_Capture : NetworkBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")) {
			transform.parent = other.gameObject.transform;
			transform.position = other.transform.position + new Vector3(0, 1, 0);
			this.enabled = false;
		}
	}
}
