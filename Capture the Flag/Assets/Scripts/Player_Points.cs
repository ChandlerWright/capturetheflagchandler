﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Player_Points : NetworkBehaviour {

	public int currentPoints;

	// Use this for initialization
	void Start () {
		if(!isLocalPlayer)
		{
			return;
		}
		currentPoints = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentPoints > 1) {
			GameOver ();
		}
	}

	void GameOver(){
		GameObject.Find ("NetworkManager").GetComponent<NetworkManager> ().ServerChangeScene ("GameOverScene");
	}
}
