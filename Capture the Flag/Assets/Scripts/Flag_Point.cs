﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Flag_Point : NetworkBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Goal")){
			Player_Points pPoints = GetComponentInParent<Player_Points> ();
			gameObject.GetComponent<Flag_Spawn> ().Respawn ();
			pPoints.currentPoints++;
		}
	}
}
