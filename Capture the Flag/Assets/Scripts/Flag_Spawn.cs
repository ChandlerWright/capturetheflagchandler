﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Flag_Spawn : NetworkBehaviour {

	public GameObject[] spawnPoints;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Respawn(){
		transform.parent = null;
		transform.position = spawnPoints [Random.Range (0, spawnPoints.Length)].transform.position;
		gameObject.GetComponent<Flag_Capture> ().enabled = true;
	}
}
