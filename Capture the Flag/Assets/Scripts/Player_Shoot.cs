﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// @Author: Bradley Cesario, 10/26/2017
/// The purpose of class Player_Shoot is to allow a bullet prefab to be 
/// spawned by the server, onto the player who requested the command, bulletSpawn
/// transform. It is then reflected across all clients.
/// </summary>
public class Player_Shoot : NetworkBehaviour
{
    //bullet prefab
    public GameObject bulletPrefab;
    //bullet spawn position
    public Transform bulletSpawn;
    public float bulletSpeed;

    void Update()
    {
        //If not local player don't do anything
        if(!isLocalPlayer)
        {
            return;
        }

        //Player left clicks on the mouse
        if(Input.GetButtonDown("Fire1"))
        {
            CmdShoot();
        }
    }
    //Request the server to spawn a bullet gameObject.
    [Command]
    void CmdShoot()
    {
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * bulletSpeed;
        NetworkServer.Spawn(bullet);
        Destroy(bullet, 2);
    }
}
